<?php
/**
 *
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 * Template Name: Campaigns Detail Page
 */

get_header(); ?>
<section class="single-col page-content primary" role="main">
		
	        <div class="container_full content_band">
	        	<div class="container_boxed--narrow">
	        		<?php the_field('1col_content_area');?>
	        	</div>
	        </div>

	        <article class="container_full content_band">
	        	<div class="container_boxed--narrow">
	        		<?php the_field('1col_content_area_2');?>
	        	</div>
	        </article>
			
			<div class="container_full">
			<?php

			// check if the repeater field has rows of data
			if( have_rows('campaign_item') ):?>
			

			    <?php while ( have_rows('campaign_item') ) : the_row();?>
				
				<div class="campaign-item">
					<div class = "campaign-item__image">
					   <?php
							the_sub_field('campaign_media');
						?>
					</div>
						        	
					<div class="campaign-item__content container_boxed--narrow content_band--small">
						<?php
							the_sub_field('campaign_content');
						?>
					</div>

				</div>
						       
			    <?php endwhile;?>
			</div>
			<?php 

			else :

			    // no rows found

			endif;

			?>

			<div class="container_full splash-content-block">
	        	<div class = "testimonial-image-block splash-image-narrow image_fullwidth" style="background:linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('<?php the_field('testimonial_area_image'); ?>');">
		        	<div class="splash-content-overlay container_boxed--narrow center text-reverse">
		        		<?php
							the_field('testimonial_content');
						?>
		        	</div>
		        </div>
		    </div>

	        <?php

			// check if the repeater field has rows of data
			if( have_rows('image_grid_general') ):?>
			<div class="container_full">	

			    <?php while ( have_rows('image_grid_general') ) : the_row();?>
				
				<div class="image-grid-block">
				        	<?php if( get_sub_field('tile_link_destination') ): ?>
				        	<a href="<?php the_sub_field('tile_link_destination');?>" class="">
				        	<?php endif ?>
					        	<div class = "image-grid-item" style="background:linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url('<?php the_sub_field('grid_image'); ?>');">
						        	<div class="grid-content-container center text-reverse">
						        		<div class="grid-content">
							        	<?php
							        	the_sub_field('tile_text');
							        	?>
							        	</div>

							        	<div class="grid-dynamic__container">
								        	<div class="grid-dynamic">
								        	<?php
								        	the_sub_field('dynamic_content');
								        	?>
								        	</div>
								        </div>
						        	</div>
						        </div>
						    <?php if( get_sub_field('tile_link_destination') ): ?>    
					    	</a>
					    	<?php endif ?>
				</div>
			       

			    <?php endwhile;?>
			</div>    
			<?php 

			else :

			    // no rows found

			endif;

			?>

			<aside class="container_boxed content_band--lined">
	        	<div class="container_boxed--narrow content_band--small">
	        		<?php the_field('footer_area');?>
	        	</div>
	        </aside>
	
</section>

<?php get_footer(); ?>
