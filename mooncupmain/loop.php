<?php
/**
 * Mooncup Main template for displaying the standard Loop
 *
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<div class="single-post">
		<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
			<div class="">
				<?php if (has_post_thumbnail( $post->ID ) ): ?>
					<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
		        	<div class = "content post-featured-image image-cover" style="background-image:url('<?php echo $image[0]; ?>');"></div>
			    <?php endif; ?>
			
			</div>
		</a>
		
		<div class="post-content">
			<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
				<h1 class="post-title"><?php

					if ( is_singular() ) :
						the_title();
					else : ?>

						<?php
							the_title(); ?>
						<?php

					endif; ?>

				</h1>
			</a>
		
		

				<div class="post-meta"><?php
					mooncupmain_post_meta(); ?>
				</div>

					<?php if ( is_front_page() || is_category() || is_archive() || is_search() ) : ?>

						<?php the_excerpt(); ?>
						<a href="<?php the_permalink(); ?>" class="read-more"><?php _e( 'Read more', 'mooncupmain' ); ?></a>

					<?php else : ?>

						<?php the_content( __( 'Continue reading &raquo', 'mooncupmain' ) ); ?>

					<?php endif; ?>

					<?php
						wp_link_pages(
							array(
								'before'           => '<div class="linked-page-nav"><p>'. __( 'This article has more parts: ', 'mooncupmain' ),
								'after'            => '</p></div>',
								'next_or_number'   => 'number',
								'separator'        => ' ',
								'pagelink'         => __( '&lt;%&gt;', 'mooncupmain' ),
							)
						);
					?>

		</div>
	</div>


</article>