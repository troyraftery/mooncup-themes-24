<?php
/**
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 * Template Name: Blog Landing Page
 */

get_header(); ?>

<div class="container_full">
		<?php

			/*
			*  Loop through post objects (assuming this is a multi-select field) ( setup postdata )
			*  Using this method, you can use all the normal WP functions as the $post object is temporarily initialized within the loop
			*  Read more: http://codex.wordpress.org/Template_Tags/get_posts#Reset_after_Postlists_with_offset
			*/

			$post_objects = get_field('post_objects');

			if( $post_objects ): ?>
			    <ul>
			    <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
			        <?php setup_postdata($post); ?>
			        <li>
			            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			            <span><?php the_excerpt();?></span>
			        </li>
			    <?php endforeach; ?>
			    </ul>
			    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			<?php endif;

			/*
			*  Loop through post objects (assuming this is a multi-select field) ( don't setup postdata )
			*  Using this method, the $post object is never changed so all functions need a seccond parameter of the post ID in question.
			*/

			$post_objects = get_field('post_objects');

			if( $post_objects ): ?>
			    <ul>
			    <?php foreach( $post_objects as $post_object): ?>
			        <li>
			            <a href="<?php echo get_permalink($post_object->ID); ?>"><?php echo get_the_title($post_object->ID); ?></a>
			            <span>Post Object Custom Field: hello</span>
			        </li>
			    <?php endforeach; ?>
			    </ul>
			<?php endif;

			?>
</div>
<section class="single-col page-content primary" role="main">
			<article class="container_full splash-content-block">
		        	<div class = "splash-image-narrow splash-image_generic image_fullwidth" style="background-image:url('<?php the_field('splash_image'); ?>');">
			        	<div class="splash-content-overlay splash-header text-reverse">
			        		<div class="container_full">
				        	<?php the_field('splash_content'); ?>
				        	</div>
			        	</div>
			        </div>
			</article>

		    <div class="container_boxed content_band--small">
		    	<div class="col__8">
			    	<div class="featured-post">
			    		<div class="blog-post">
			    			<?php

							$post_object = get_field('main_feature_post');

							if( $post_object ): 

								// override $post
								$post = $post_object;
								setup_postdata( $post ); 

								?>

							<a href="<?php the_permalink(); ?>" rel="bookmark">
								<div class="blog-image">
									<?php if (has_post_thumbnail( $post->ID ) ): ?>
										<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
							        	<div class = "content post-featured-image image-cover" style="background-image:url('<?php echo $image[0]; ?>');"></div>
								    <?php endif; ?>
								</div>
							</a>

								<div class="post-content">
									<a href="<?php the_permalink(); ?>" rel="bookmark">
										<h1 class="post-title"><?php the_title(); ?></h1>
									</a>
							
										<!--<div class="post-info small caps"><?php the_author(); ?> / <?php echo get_the_date(); ?></div>-->
								
										<?php the_excerpt(); ?>
										<a href="<?php the_permalink(); ?>" class="read-more"><?php _e( 'Read more', 'mooncupmain' ); ?></a>
										
										<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
										<?php endif; ?>
								</div>
						</div>
					</div>
				

					<section class="featured-post-listing blog-section">
						
						<div class="container">
						<?php

							$post_objects = get_field('featured_items_all');

							if( $post_objects ): ?>
							    
							    <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
							        <?php setup_postdata($post); ?>
							        <article class="blog-post">
						    		

										<a href="<?php the_permalink(); ?>" rel="bookmark">
											<div class="blog-image">
												<?php if (has_post_thumbnail( $post->ID ) ): ?>
													<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
										        	<div class = "content post-featured-image image-cover" style="background-image:url('<?php echo $image[0]; ?>');"></div>
											    <?php endif; ?>
											</div>
										</a>

											<div class="post-content">
												<a href="<?php the_permalink(); ?>" rel="bookmark">
													<h1 class="post-title"><?php the_title(); ?></h1>
												</a>
												<!--<div class="post-info small caps"><?php the_author(); ?> / <?php echo get_the_date(); ?></div>-->
												<?php the_excerpt(); ?>
												<a href="<?php the_permalink(); ?>" class="read-more"><?php _e( 'Read more', 'mooncupmain' ); ?></a>
													
													
											</div>
									</article>
							    <?php endforeach; ?>
							    
							    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
							<?php endif;?>	
						</div>
			 		</section>   
			 	
			 	<!--<div class="container_boxed blog-section">
			 		<section class="category-block col__6">
						<div class="category-title">
							<h1><?php the_field('category_title_1');?></h1>
							<div class="category-link">
								<a href="/blogs/<?php the_field('category_link_1');?>" class="btn-primary-outline">Read more</a>
							</div>
						</div>
						<div class="container">
						<?php

							$post_objects = get_field('category_item_1');

							if( $post_objects ): ?>
							    
							    <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
							        <?php setup_postdata($post); ?>
							        <article class="blog-post">
						    		

										<a href="<?php the_permalink(); ?>" rel="bookmark">
											<div class="blog-image">
												<?php if (has_post_thumbnail( $post->ID ) ): ?>
													<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
										        	<div class = "content post-featured-image image-cover" style="background-image:url('<?php echo $image[0]; ?>');"></div>
											    <?php endif; ?>
											</div>
										</a>

											<div class="post-content">
												<a href="<?php the_permalink(); ?>" rel="bookmark">
													<h1 class="post-title"><?php the_title(); ?></h1>
												</a>
												<div class="post-info small caps"><?php the_author(); ?> / <?php echo get_the_date(); ?></div>

												<?php the_excerpt(); ?>
												<a href="<?php the_permalink(); ?>" class="read-more"><?php _e( 'Read more', 'mooncupmain' ); ?></a>
													
													
											</div>
									</article>
							    <?php endforeach; ?>
							    
							    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
							<?php endif;?>	
						</div>
			 		</section>  
			 	</div>-->
			 </div>
			 <aside class="blog-sidebar col__4">
		        <ul class="blog-sidebar"><?php
					if ( function_exists( 'dynamic_sidebar' ) ) :
						dynamic_sidebar( 'blog-sidebar' );
					endif; ?>
				</ul>	
		    </aside>
		</div><!--end of container--> 		

	       
	
</section>

<?php get_footer(); ?>
