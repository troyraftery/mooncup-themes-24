<?php
/**
 *
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 * Template Name: Why Mooncup Main
 */

get_header(); ?>
<section class="why-mooncup page-content primary" role="main">
		
<?php
// check if the flexible content field has rows of data
if( have_rows('why_MC_block') ):?>

	
		<?php

	     // loop through the rows of data
	    while ( have_rows('why_MC_block') ) : the_row();?>
			
	        <?php if( get_row_layout() == 'image_' ):?>
	        <div class="container_full splash-content-block">
	        	<div class = "splash-image-narrow image_fullwidth" style="background-image:url('<?php the_sub_field('image_fullwidth'); ?>');">
		        	<div class="splash-content-overlay center text-reverse">
		        		<div class="container_boxed--narrow">
			        	<?php
			        	the_sub_field('image_overlay');
			        	?>
			        	</div>
		        	</div>
		        </div>

	        	<article class="container_full content_band">
	        		<div class="container_boxed--narrow">
	        	<?php
	        	the_sub_field('content_area');?>
	        		</div>
	        	</article>
	        </div>

	        <?php elseif( get_row_layout() == 'content_over_image_block' ):?>
	        <article class="container_full content-overimage-block">
	        	<div class = "splash-image-narrow image_fullwidth" style="background-image:url('<?php the_sub_field('image_fullwidth_2'); ?>');">
		        	<div class="splash-content-overlay center text-reverse">
		        		<div class="container_boxed--narrow content_band">
			        	<?php
			        	the_sub_field('content_overlay');
			        	?>
			        	</div>
		        	</div>
		        </div>
		    </article>

	        	<?php endif;

	    endwhile;

	else :
	    echo 'no layouts found';
	endif;
	?>
	

	
</section>

<?php get_footer(); ?>
